function burger_menu() {
    document.getElementsByTagName("header")[0].classList.toggle("menu-active");
}

let ourHistory = document.querySelector(".our-history");
ourHistory.addEventListener('click', (e) => {
	init();
});

let menuOptions = document.querySelectorAll('.overlay-menu a');
menuOptions.forEach((menuOption) => {
	menuOption.addEventListener('click', (e) => {
		burger_menu();
	});
});

document.addEventListener('scroll', () => {
	let isOpen = document.getElementsByTagName("header")[0].classList.contains("menu-active");
	if (isOpen) {
		burger_menu();
	}
});