<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta property="og:url"             content="https://www.evolvemediallc.com/" /> 
<meta property="og:title"           content="Evolve Media, LLC." /> 
<meta property="og:image"           content="/images/og-facebook.png" /> 
<meta property="og:description" content="Evolve Media is a publisher of leading enthusiast destinations for men and women." />
<title>Evolve Media, LLC.</title>
<meta name="description" content="Evolve Media is a publisher of leading enthusiast destinations for men and women." />
<link rel="canonical" href="https://www.evolvemediallc.com/" />
<link rel="apple-touch-icon" sizes="57x57" href="/images/setup/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/images/setup/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/images/setup/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/images/setup/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/images/setup/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/images/setup/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/images/setup/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/images/setup/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/images/setup/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192" href="/images/setup/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="144x144" href="/images/setup/android-icon-144x144.png">
<link rel="icon" type="image/png" sizes="96x96" href="/images/setup/android-icon-96x96.png">
<link rel="icon" type="image/png" sizes="72x72" href="/images/setup/android-icon-72x72.png">
<link rel="icon" type="image/png" sizes="48x48" href="/images/setup/android-icon-48x48.png">
<link rel="icon" type="image/png" sizes="36x36" href="/images/setup/android-icon-36x36.png">
<link rel="icon" type="image/png" sizes="32x32" href="/images/setup/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/images/setup/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/images/setup/favicon-16x16.png">
<link rel="manifest" href="manifest.json">
<link rel="shortcut icon" href="/images/setup/favicon.ico">
<link rel="apple-touch-icon" href="/images/setup/apple-icon.png">
<link rel="apple-touch-icon-precomposed" href="/images/setup/apple-icon-precomposed.png">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Libre+Franklin:wght@400;500&display=swap" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/dist/main.css"/>
</head>
<body class="evolve-brands">
<header>
	<div class="header-container">
		<img src="images/evolve-logo-white.svg" class="header-logo" alt="Evolve Media LLC logo">
		<div class="burger" onclick="burger_menu()">
			<div class="lines"></div>
		</div>
	</div>
	
	<div class="overlay-header">
		<div class="overlay-background"></div>
		<div class="overlay-menu">
			<ul>
				<li><a class="home" href="/#">Home</a></li>
				<li><a class="our-brands" href="/#our-brands">Our Brands</a></li>
				<li><a class="our-history" href="/#our-history">Our History</a></li>
				<li><a class="our-leadership" href="/#our-leadership">Our Leadership</a></li>
				<li><a class="contact-us" href="/#contact-us">Contact Us</a></li>
			</ul>
		</div>
	</div>
</header>
<div class="wrapper">
	<div class="evolve-brands-container">

		<div class="homepage-main-image">
			<img src="/images/CP/evolve_media_our_brands_header_image.jpg" alt="Main Header">
		</div>

		<h2 class="headline">Our Brands</h2>

		<div class="brands-container">
			
			<div class="brand">
				<div class="brand-logo"><img src="/images/our-brands/comingsoon_logo_color.svg" /></div>
				<div class="brand-description">Whether you're looking for a new television show to binge or want to hear directly from Hollywood's biggest directors, ComingSoon editors provide exclusive content and top-notch news coverage daily.</div>
			</div>

			<div class="brand">
				<div class="brand-logo"><img src="/images/our-brands/superherohype_logo_color.svg" /></div>
				<div class="brand-description">The cape-wearing team of editors at SuperHeroHype aim to provide up-to-the-minute news and an insider look into the universe of comics, superheroes, fantasy, sci-fi, cosplay and geek culture written by superfans for the highest level of authenticity.</div>
			</div>
			
			<div class="brand">
				<div class="brand-logo"><img src="/images/our-brands/gamerevolution_logo_color.svg" /></div>
				<div class="brand-description">A staple of games journalism since 1996, multiple generations of gamers know GameRevolution as the one-stop destination for breaking news, reviews, previews, and guides produced by its veteran team of gaming experts.</div>
			</div>

			<div class="brand">
				<div class="brand-logo"><img src="/images/our-brands/psls_logo_color.svg" /></div>
				<div class="brand-description">Controller in one hand, keyboard in the other, PlayStation LifeStyle's expertise in writing about games comes from passion and experience.</div>
			</div>

			<div class="brand">
				<div class="brand-logo"><img src="/images/our-brands/sherdog_logo_color.svg" /></div>
				<div class="brand-description">Serving up heaping fistfuls of mixed martial arts since 1997, Sherdog’s hard-hitting journalism pulls no punches even with the world’s fiercest athletes.</div>
			</div>

			<div class="brand">
				<div class="brand-logo"><img src="/images/our-brands/wrestlezone_logo_color.svg" /></div>
				<div class="brand-description">Now entering the ring… WrestleZone takes a championship approach to covering the exciting and dramatic world of professional wrestling with news, interviews, features and more.</div>
			</div>

			<div class="brand">
				<div class="brand-logo"><img src="/images/our-brands/hfboards_logo_color.svg" /></div>
				<div class="brand-description">The Internet’s largest and long-running community for discussion surrounding ice hockey, including coverage of the NHL, college teams, and anywhere else the sport is played globally.</div>
			</div>

			<div class="brand">
				<div class="brand-logo"><img src="/images/our-brands/liveoutdoors_logo_color.svg" /></div>
				<div class="brand-description">LiveOutdoors reconnects individuals with nature, and equips them with the knowledge and confidence to explore and embrace the world around them.</div>
			</div>


			<div class="brand">
				<div class="brand-logo"><img src="/images/our-brands/mandatory_logo_color.svg" /></div>
				<div class="brand-description">Mandatory engages young men with content and stories that entertain and inspire while urging our readers, whenever possible, to get actively involved and do some good. Mandatory now means what it says: Showing up isn’t optional anymore.</div>
			</div>

			<div class="brand">
				<div class="brand-logo"><img src="/images/our-brands/musicfeeds_logo_color.svg" /></div>
				<div class="brand-description">As Australia’s most visited music news website, Music Feeds' team of experienced music journalists with strong connections to everyone who’s anyone in the industry is dedicated to bringing audiences up to the minute music news stories, compelling features, music reviews, festival coverage and interviews with some of the biggest artists in the world.</div>
			</div>

			<div class="brand">
				<div class="brand-logo"><img src="/images/our-brands/totalbeauty_logo_color.svg" /></div>
				<div class="brand-description">The one-stop source for all things beauty. From skin care to hair care to makeup, TotalBeauty has it completely covered. </div>
			</div>

			<div class="brand">
				<div class="brand-logo"><img src="/images/our-brands/thefashionspot_logo_color.svg" /></div>
				<div class="brand-description">The spot for the latest fashion trends, beauty must-haves and wellness tips. Because everyone deserves to live their most stylish life.</div>
			</div>

			<div class="brand">
				<div class="brand-logo"><img src="/images/our-brands/momtastic_logo_color.svg" /></div>
				<div class="brand-description">United by motherhood, the team at Momtastic delivers personal stories, life hacks, pregnancy and parenting tips, expert advice and more to a diverse group of moms and moms-to-be.</div>
			</div>

			<div class="brand">
				<div class="brand-logo"><img src="/images/our-brands/realitytea_logo_color.svg" /></div>
				<div class="brand-description">Always ready to serve up a cup of tea on the hottest reality TV superstars and the shows they're on, RealityTea's got celebrity rumors, gossip, news and more.</div>
			</div>

			<div class="brand">
				<div class="brand-logo"><img src="/images/our-brands/dogtime_logo_color.svg" /></div>
				<div class="brand-description">The team at DogTime is a different breed with the information pet parents need to keep their dogs healthy from puppyhood to senior years, plenty of ways to celebrate all things canine, and tips for getting involved in the rescue and shelter communities.</div>
			</div>

			<div class="brand">
				<div class="brand-logo"><img src="/images/our-brands/cattime_logo_color.svg" /></div>
				<div class="brand-description">CatTime editors use their paws-on experience to provide pet parents with everything they need to take lifelong care of their felines, show their love for cats all year long, and help shelters and rescues get cats into loving forever homes.</div>
			</div>

			<div class="cta-btn">
				<a class="button" href="/">Return to Home</a>
			</div>

		</div>

	</div>
</div>
<script src="js/main.js"></script>
<?php include_once "footer.php"; ?>
</body>
</html>